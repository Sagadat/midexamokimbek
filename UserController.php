<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\User;
use Redirect;
use Input;
class UserController extends Controller{

	public function index(){
		
		$User = User::paginate(5);
		return view('User_index', compact('User') );	
	}
	public function create(){
		return view('User_create');
	}
	// insert to DB here
	public function store(Request $request){	
		$User            = new User();
		$User  -> name    = $request -> User_name;
		$User  -> address = $request -> User_address;
		$User  -> save();
		return redirect() -> route('User.index');	
	}
	public function edit($id)
	{
		$User = User::find($id);
		return view('User_edit', compact('User'));
	}
	public function update(Request $request, $id)
	{
		$User = User::find($id);
		$User -> name    = $request -> User_name;
		$User -> address = $request -> User_address;
		$User -> save();
		return redirect() -> route('User.index');	
	}
	public function destroy($id)
	{
		$User = User::find($id);
		$User -> delete();
		return redirect() -> route('User.index');
	}
}
?>
